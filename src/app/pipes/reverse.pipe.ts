import { Container } from 'src/app/models/container';
import { Pipe, PipeTransform } from '@angular/core';
import { CachedResourceLoader } from '@angular/platform-browser-dynamic/src/resource_loader/resource_loader_cache';

@Pipe({
    name: 'reverse',
    pure: false
})
export class ReversePipe implements PipeTransform {
    cached: Container[][];
    transform(value) {
        let check = true;
        if (this.cached) {
            for (let i = 0; i < value.length; i++) {
                for (let j = 0; j < value[i].length; j++) {
                    if (value[i][j] !== this.cached[i][j]) {
                        check = false;
                        break;
                    }
                }
            }
        } else {
            check = false;
        }
        if (!check) {
            this.cached = JSON.parse(JSON.stringify(value.reverse()));
        }

        return this.cached;
    }
}
