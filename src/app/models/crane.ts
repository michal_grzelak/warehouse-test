import { Container } from './container';

export class Crane {
    constructor() {
        this.currentPosition = 0;
    }
    container: Container;
    currentPosition: number;
}
