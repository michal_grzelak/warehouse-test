export class Container {

    constructor(weight: number, durability: number) {
        this.weight = weight;
        this.durability = durability;
    }

    weight: number;
    durability: number;
}
