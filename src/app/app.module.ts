import { ReversePipe } from './pipes/reverse.pipe';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { WarehouseComponent } from './warehouse/warehouse.component';
import { CraneComponent } from './warehouse/crane/crane.component';
import { StoragePlaceComponent } from './warehouse/storage-place/storage-place.component';
import { PipeModule } from './pipe.module';
import { OrderModule } from 'ngx-order-pipe';
import {MatTooltipModule} from '@angular/material/tooltip';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    WarehouseComponent,
    CraneComponent,
    StoragePlaceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PipeModule,
    OrderModule,
    BrowserAnimationsModule,
    MatTooltipModule
  ],
  providers: [ReversePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
