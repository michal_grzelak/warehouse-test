import { StoragePlaceComponent } from './storage-place/storage-place.component';
import { CraneComponent } from './crane/crane.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {MatTooltipModule} from '@angular/material/tooltip';

import { WarehouseComponent } from './warehouse.component';

describe('WarehouseComponent', () => {
  let component: WarehouseComponent;
  let fixture: ComponentFixture<WarehouseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseComponent, CraneComponent, StoragePlaceComponent ],
      imports: [MatTooltipModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
