import { Observable, of, Subject } from 'rxjs';
import { Container } from 'src/app/models/container';
import { Injectable } from '@angular/core';

import { delay, take } from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class WarehouseService {

  unloadCraneSubject: Subject<any> = new Subject<any>();
  loadCraneSubject: Subject<number> = new Subject<number>();
  takeFromStorageSubject: Subject<Container> = new Subject<Container>();
  putToStorageCraneSubject: Subject<boolean> = new Subject<boolean>();
  storageLengthSubject: Subject<number> = new Subject<number>();
  storageLengthRequestSubject: Subject<void> = new Subject<void>();

  constructor() { }


  loadCrane(position: number): Promise<Container> {
    const result = this.takeFromStorageSubject.pipe(take(1)).toPromise();

    this.loadCraneSubject.next(position);

    return result;
  }

  unloadCrane(container: Container, position: number): Promise<boolean> {
    const result = this.putToStorageCraneSubject.pipe(take(1)).toPromise();

    this.unloadCraneSubject.next({ container: container, position: position });

    return result;
  }

  getStorageLength(): Promise<number> {
    const result = this.storageLengthSubject.pipe(take(1)).toPromise();
    this.storageLengthRequestSubject.next();
    return result;
  }
}
