import { ReversePipe } from 'src/app/pipes/reverse.pipe';
import { Container } from 'src/app/models/container';
import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { WarehouseService } from 'src/app/warehouse/warehouse.service';

@Component({
  selector: 'app-storage-place',
  templateUrl: './storage-place.component.html',
  styleUrls: ['./storage-place.component.css']
})
export class StoragePlaceComponent implements OnInit {

  storage: Array<Array<Container>>;

  constructor(
    private _warehouseService: WarehouseService,
    private cdref: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.storage = new Array<Array<Container>>(4);

    for (let i = 0; i < this.storage.length; i++) {
      this.storage[i] = new Array<Container>(7);
    }

    this.storage = JSON.parse(JSON.stringify(this.storage));

    // subscribe to load/unload/move crane
    this._warehouseService.unloadCraneSubject.subscribe((data) => {
      const result = this.put(data.container, data.position);
      this._warehouseService.putToStorageCraneSubject.next(result);
    });

    this._warehouseService.loadCraneSubject.subscribe((data) => {
      const result = this.take(data);
      this._warehouseService.takeFromStorageSubject.next(result);
    });

    this._warehouseService.storageLengthRequestSubject.subscribe(() => {
      const result = this.getStorageLength();
      this._warehouseService.storageLengthSubject.next(result);
    });
  }

  getStorageLength(): number {
    if (this.getStorageHeight() > 0) {
      return this.storage[0].length;
    } else {
      return 0;
    }
  }

  getStorageHeight(): number {
    return this.storage.length;
  }

  columnExists(position: number): boolean {
    const length = this.getStorageLength();

    if (position < 0 || position >= length) {
      return false;
    }

    return true;
  }

  rowExists(position: number): boolean {
    const height = this.getStorageHeight();

    if (position < 0 || position >= height) {
      return false;
    }

    return true;
  }

  getAllowedWeightInPosition(x: number, y: number) {
    if (!this.positionExists(x, y)) {
      return -1;
    }

    const height = this.getContainersInColumnCount(x);
    if (y > this.getStorageHeight() - 1 - height) {
      return -1;
    }

    if (y === this.getStorageHeight() - 1) {
      return Number.MAX_SAFE_INTEGER;
    }

    let allowedWeight = Number.MAX_SAFE_INTEGER;
    for (let i = this.getStorageHeight() - 2; i >= y; i--) {
      const weight = this.getTotalWeightFrom(x, i);
      const allowed = this.storage[i + 1][x].durability - weight;

      if (allowed < allowedWeight) {
        allowedWeight = allowed;
      }
    }

    return allowedWeight;
  }

  getTotalWeightFrom(x: number, y: number) {
    if (!this.positionExists(x, y)) {
      return -1;
    }

    const height = this.getContainersInColumnCount(x);
    if (y < this.getStorageHeight() - 1 - height) {
      return 0;
    }

    let weight = 0;

    for (let i = y; i > this.getStorageHeight() - 1 - height; i--) {
      weight += this.storage[i][x].weight;
    }

    return weight;
  }

  getContainersInColumnCount(position: number) {
    if (!this.columnExists(position)) {
      return -1;
    }

    let sum = 0;

    for (let i = this.getStorageHeight() - 1; i >= 0; i--) {
      if (this.storage[i][position] == null) {
        break;
      } else {
        sum++;
      }
    }

    return sum;
  }

  positionExists(x: number, y: number): boolean {
    if (!this.columnExists(x) || !this.rowExists(y)) {
      return false;
    }

    return true;
  }

  isPositionFree(x: number, y: number): boolean {
    if (!this.positionExists(x, y)) {
      return false;
    }

    return this.storage[y][x] == null;
  }

  getFirstFreePosition(x: number) {
    if (!this.columnExists(x)) {
      return -1;
    }

    const height = this.getStorageHeight();
    if (height <= 0) {
      return -1;
    }

    const count = this.getContainersInColumnCount(x);
    if (height === count) {
      return -1;
    } else {
      return height - count - 1;
    }
  }



  take(position: number): Container {
    const count = this.getContainersInColumnCount(position);
    if (!this.columnExists(position) || count === 0) {
      return null;
    }

    const container = this.storage[this.getStorageHeight() - count][position];
    this.storage[this.getStorageHeight() - count][position] = null;

    return container;
  }

  put(container: Container, position: number): boolean {
    if (!this.columnExists(position)) {
      return false;
    }

    const height = this.getStorageHeight();
    if (height <= 0) {
      return false;
    }

    const count = this.getContainersInColumnCount(position);

    if (count === 0) {
      this.storage[height - 1][position] = container;
      return true;
    } else if (count === height) {
      return false;
    } else {
      const freePos = this.getFirstFreePosition(position);

      const allowedWeight = this.getAllowedWeightInPosition(position, freePos);

      if (allowedWeight >= container.weight) {
        this.storage[freePos][position] = container;
        return true;
      } else {
        return false;
      }
    }
  }
}
