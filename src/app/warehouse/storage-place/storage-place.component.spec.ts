import { Container } from 'src/app/models/container';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatTooltipModule } from '@angular/material/tooltip';

import { StoragePlaceComponent } from './storage-place.component';

describe('StoragePlaceComponent', () => {
  let component: StoragePlaceComponent;
  let fixture: ComponentFixture<StoragePlaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StoragePlaceComponent],
      imports: [MatTooltipModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoragePlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('height and length check', () => {
    expect(component.getStorageLength()).toBe(7);
    expect(component.getStorageHeight()).toBe(4);

    expect(component.columnExists(-1)).not.toBeTruthy();
    for (let i = 0; i < component.getStorageLength(); i++) {
      expect(component.columnExists(i)).toBeTruthy();
    }
    expect(component.columnExists(7)).not.toBeTruthy();
  });

  it('empty warehouse conditions check', () => {
    // underflow
    expect(component.getFirstFreePosition(-1)).toBe(-1);
    expect(component.getContainersInColumnCount(-1)).toBe(-1);
    expect(component.getAllowedWeightInPosition(-1, 0)).toBe(-1);
    expect(component.getTotalWeightFrom(-1, 0)).toBe(-1);

    for (let i = 0; i < component.getStorageLength(); i++) {
      expect(component.getFirstFreePosition(i)).toBe(3);
      expect(component.getContainersInColumnCount(i)).toBe(0);
      expect(component.getAllowedWeightInPosition(i, 3)).toBe(Number.MAX_SAFE_INTEGER);
      expect(component.getTotalWeightFrom(i, 3)).toBe(0);
    }

    // overflow
    expect(component.getFirstFreePosition(7)).toBe(-1);
    expect(component.getContainersInColumnCount(7)).toBe(-1);
    expect(component.getAllowedWeightInPosition(7, 0)).toBe(-1);
    expect(component.getTotalWeightFrom(7, 0)).toBe(-1);
  });

  it('container putting check', () => {
    const container11 = new Container(1, 1);
    const container12 = new Container(1, 2);
    const container22 = new Container(2, 2);
    const container23 = new Container(2, 3);

    // position x = -1
    expect(component.put(container12, -1)).not.toBeTruthy();
    expect(component.getAllowedWeightInPosition(-1, 0)).toBe(-1);

    // position x = 7
    expect(component.put(container12, 7)).not.toBeTruthy();
    expect(component.getAllowedWeightInPosition(7, 0)).toBe(-1);

    // position x = 0
    expect(component.put(container12, 0)).toBeTruthy();
    expect(component.getAllowedWeightInPosition(0, component.getStorageHeight() - 2)).toBe(2);
    expect(component.getContainersInColumnCount(0)).toBe(1);
    expect(component.put(container11, 0)).toBeTruthy();
    expect(component.getContainersInColumnCount(0)).toBe(2);
    expect(component.getAllowedWeightInPosition(0, component.getStorageHeight() - 3)).toBe(1);
    expect(component.put(container22, 0)).not.toBeTruthy();
    expect(component.put(container11, 0)).toBeTruthy();
    expect(component.getContainersInColumnCount(0)).toBe(3);

    // position x = 1
    expect(component.put(container23, 1)).toBeTruthy();
    expect(component.getContainersInColumnCount(1)).toBe(1);
    expect(component.getAllowedWeightInPosition(1, component.getStorageHeight() - 2)).toBe(3);
    expect(component.put(container12, 1)).toBeTruthy();
    expect(component.getContainersInColumnCount(1)).toBe(2);
    expect(component.getAllowedWeightInPosition(1, component.getStorageHeight() - 3)).toBe(2);
    expect(component.put(container22, 1)).toBeTruthy();
    expect(component.getContainersInColumnCount(1)).toBe(3);
    expect(component.put(container11, 1)).not.toBeTruthy();
  });

  it('container taking check', () => {
    const container11 = new Container(1, 1);
    const container12 = new Container(1, 2);
    const container22 = new Container(2, 2);
    const container23 = new Container(2, 3);

    // position x = 0
    component.put(container12, 0);
    component.put(container11, 0);
    component.put(container11, 0);

    // position x = 1
    component.put(container23, 1);
    component.put(container12, 1);
    component.put(container22, 1);

    // position x = 0
    expect(component.getContainersInColumnCount(0)).toBe(3);
    expect(component.take(0)).toBe(container11);
    expect(component.getContainersInColumnCount(0)).toBe(2);
    expect(component.take(0)).toBe(container11);
    expect(component.getContainersInColumnCount(0)).toBe(1);
    expect(component.take(0)).toBe(container12);
    expect(component.getContainersInColumnCount(0)).toBe(0);
    expect(component.take(0)).toBe(null);
    expect(component.getContainersInColumnCount(0)).toBe(0);

    // position x = 1
    expect(component.getContainersInColumnCount(1)).toBe(3);
    expect(component.take(1)).toBe(container22);
    expect(component.getContainersInColumnCount(1)).toBe(2);
    expect(component.take(1)).toBe(container12);
    expect(component.getContainersInColumnCount(1)).toBe(1);
    expect(component.take(1)).toBe(container23);
    expect(component.getContainersInColumnCount(1)).toBe(0);
    expect(component.take(1)).toBe(null);
    expect(component.getContainersInColumnCount(1)).toBe(0);
  });
});
