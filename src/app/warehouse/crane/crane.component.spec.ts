import { WarehouseService } from 'src/app/warehouse/warehouse.service';
import { ReversePipe } from '../../pipes/reverse.pipe';
import { StoragePlaceComponent } from './../storage-place/storage-place.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatTooltipModule } from '@angular/material/tooltip';

import { CraneComponent } from './crane.component';

describe('CraneComponent', () => {
  let component: CraneComponent;
  let fixture: ComponentFixture<CraneComponent>;
  let storageComponentFixture: ComponentFixture<StoragePlaceComponent>;
  let storageComponent: StoragePlaceComponent;
  let originalTimeout;
  let storageLength: number;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CraneComponent, StoragePlaceComponent, ReversePipe],
      imports: [MatTooltipModule]
    })
      .compileComponents();
  }));

  beforeEach(async () => {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;

    fixture = TestBed.createComponent(CraneComponent);
    storageComponentFixture = TestBed.createComponent(StoragePlaceComponent);
    storageComponent = storageComponentFixture.componentInstance;
    storageComponentFixture.detectChanges();
    component = fixture.componentInstance;
    fixture.detectChanges();
    const service: WarehouseService = TestBed.get(WarehouseService);
    storageLength = await service.getStorageLength();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('load / unload check', async () => {
    expect(component.isLoaded()).not.toBeTruthy();

    expect(await component.mockCreateContainer()).toBeTruthy();
    expect(component.isLoaded()).toBeTruthy();

    expect(await component.load()).not.toBeTruthy();

    expect(await component.unload()).toBeTruthy();
    expect(component.isLoaded()).not.toBeTruthy();
    expect(await component.unload()).not.toBeTruthy();
  });

  it('crane movement check', async () => {
    expect(await component.moveInDirection(-1)).not.toBeTruthy();
    expect(component.getCurrentPosition()).toBe(0);

    for (let i = 1; i < storageLength; i++) {
      expect(await component.moveInDirection(1)).toBeTruthy();
      expect(component.getCurrentPosition()).toBe(i);
    }


    expect(await component.moveInDirection(1)).not.toBeTruthy();
    expect(component.getCurrentPosition()).toBe(storageLength - 1);
  });
});
