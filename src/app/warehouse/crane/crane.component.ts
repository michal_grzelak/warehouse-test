import { WarehouseService } from 'src/app/warehouse/warehouse.service';
import { Crane } from 'src/app/models/crane';
import { Component, OnInit } from '@angular/core';
import { Container } from 'src/app/models/container';
import { ReversePipe } from 'src/app/pipes/reverse.pipe';

@Component({
  selector: 'app-crane',
  templateUrl: './crane.component.html',
  styleUrls: ['./crane.component.css']
})
export class CraneComponent implements OnInit {

  crane: Crane;

  constructor(
    private _warehouseService: WarehouseService
  ) { }

  ngOnInit() {
    this.crane = new Crane();
  }

  async load(): Promise<boolean> {
    if (!this.isLoaded()) {
      return this._warehouseService
        .loadCrane(this.crane.currentPosition)
        .then(result => {
          if (result) {
            this.crane.container = result;

            return true;
          }

          return false;
        }, error => {
          return false;
        });
    }

    return Promise.resolve(false);
  }

  async unload(): Promise<boolean> {
    if (this.isLoaded()) {
      return this._warehouseService
        .unloadCrane(this.crane.container, this.crane.currentPosition)
        .then(result => {
          if (result) {
            this.crane.container = null;
            return true;
          }

          return false;
        }, error => {
          return false;
        });
    }

    return Promise.resolve(false);
  }

  isLoaded(): boolean {
    return this.crane.container != null;
  }

  async moveInDirection(direction: number): Promise<boolean> {
    return this._warehouseService
      .getStorageLength()
      .then(result => {
        const length = result;

        if (this.crane.currentPosition > 0 && this.crane.currentPosition < length - 1) {
          this.crane.currentPosition += direction;
          return true;
        } else if (this.crane.currentPosition === 0 && direction === 1) {
          this.crane.currentPosition += direction;
          return true;
        } else if (this.crane.currentPosition === 6 && direction === -1) {
          this.crane.currentPosition += direction;
          return true;
        }

        return false;
      });
  }

  getCurrentPosition(): number {
    return this.crane.currentPosition;
  }

  mockCreateContainer(): boolean {
    if (!this.isLoaded()) {
      this.crane.container = new Container(this.generateRandomNumber(1, 5), this.generateRandomNumber(1, 5));
      return true;
    }

    return false;
  }

  generateRandomNumber(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
