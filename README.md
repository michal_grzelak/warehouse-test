# WarehouseApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Założenia:
1. Mamy podnośnik, który może podnosić i opuszczać kontenery do magazynu. 
2. W celach testowych kontenery są obecnie generowane poprzez naciśnięcie odpowiedniego przycisku.
3. Magazyn ma ograniczoną pojemność.
4. Pojemność jest wyrażona w postaci WxS (wysokość x szerokość).
5. Każdy z kontenerów posiada 2 wartości: wagę oraz wytrzymałość.
6. Wytrzymałość określa maksymalną wagę jaką możemy położyć na danym kontenerze.
